Question and Answer System
-----------------------------
This is a Question answering system that is developed using Apache SOLR. It indexes around 15 thousand files from wikiDumps. It uses the Word Net and the Stanford NLP packages to process the natural language queries and identify the question words and Nouns and Verbs.

The Querying algorithm is explained in detail in the report attached. The retrieved answer is output using a AJAX based UI. Several features of Solr like the Auto Spelling correction, More Like This and Auto Filling are implemented.

Take a look at the features and performance measurements of the system at https://bitbucket.org/sabharin/question-and-answering-system/src/422b47cc0aea902203c3f9930e285e80015626d1/Question%20Answer%20System/Question%20and%20Answer%20System%20Report.pdf?at=master



Mail any questions to: sabharin@buffalo.edu